---
title: "Werkdruk in de Zorg"
author: "Martine Vecht"
date: "`r format(Sys.time())`"
output: html_document
editor_options: 
  chunk_output_type: console
---

# COMPLECS overzicht {.tabset .tabset-pills}

<!----------------------------------------------------------------------------->
<!-- Start of settings (specific bit) ----------------------------------------->
<!----------------------------------------------------------------------------->

## Algemeen

Hieronder staan de COMPLECS overzichten. Open de "Logs" tab om de logs van het inladen te bekijken.

## Logs

### Setup

```{r setup, include=FALSE}

googleSheetsURL <-
  paste0("https://docs.google.com/spreadsheets/d/",
         "1GEZl5rwlnMcHvY0PTZyaezl1PlzH1odRYrsNRf07xSg");

gitLabGroup <- "openuniversity";
gitLabProject <- "im-werkdruk-in-de-zorg";

filenameBase <- "im-werkdruk-in-de-zorg";

knitr::opts_chunk$set(echo = FALSE)

ufs::quietGitLabUpdate("r-packages/behaviorchange@dev", quiet = TRUE);

```

<!----------------------------------------------------------------------------->
<!-- Start of generic bit ----------------------------------------------------->
<!----------------------------------------------------------------------------->

### Inladen en verwerken spreadsheet

```{r creating-COMPLECS-overview, message=FALSE, results='asis'}

gitLabRepo <- paste0("https://gitlab.com/", gitLabGroup,
                     "/", gitLabProject);
gitLabPages <- paste0("https://", gitLabGroup,
                      ".gitlab.io/", gitLabProject);
gitLabJobs <- paste0(gitLabRepo, "/-/jobs");

neatoLayout <-
  behaviorchange::complecs(
    googleSheetsURL,
    layout="neato",
    graph_styling = list(c("outputorder", "nodesfirst", "graph"),
                         c("overlap", "true", "graph"),
                         c("fixedsize", "false", "node")),
    silent = FALSE
  );

dotLayout <-
  behaviorchange::complecs(
    googleSheetsURL,
    layout="dot",
    graph_styling = list(c("outputorder", "nodesfirst", "graph"),
                         c("overlap", "true", "graph"),
                         c("fixedsize", "false", "node")),
    silent = TRUE
  );

dotLayout_dot_code <- DiagrammeR::generate_dot(dotLayout$output$graph);
dotLayout_graphSvg <-
  DiagrammeRsvg::export_svg(DiagrammeR::grViz(dotLayout_dot_code));
dotLayout_graphSvg <- sub(".*\n<svg ", "<svg ", dotLayout_graphSvg);

neatoLayout_dot_code <- DiagrammeR::generate_dot(neatoLayout$output$graph);
neatoLayout_graphSvg <- DiagrammeRsvg::export_svg(DiagrammeR::grViz(neatoLayout_dot_code));
neatoLayout_graphSvg <- sub(".*\n<svg ", "<svg ", neatoLayout_graphSvg);

svgPanZoom <-
  paste0(readLines(here::here("js",
                              "svg-pan-zoom.min.js"),
                   warn = FALSE), collapse="\n");

### Create stand-alone HTML files

dotLayout_html <-
  c(
    "<html>\n<head>\n<script >\n",
    svgPanZoom,
    "</script>\n</head><body>\n",
    gsub("<svg width=\"[0-9]+pt\" height=\"[0-9]+pt\"\n viewBox=",
         "<svg width=\"100%\" height=\"100%\" id=\"mainSvgFile\" viewBox=",
         dotLayout_graphSvg),
    "\n<script>",
    "var mainSvgFile = svgPanZoom('#mainSvgFile');",
    "</script>\n",
    "\n</body>\n</html>"
  );

neatoLayout_html <-
  c(
    "<html>\n<head>\n<script >\n",
    svgPanZoom,
    "</script>\n</head><body>\n",
    gsub("<svg width=\"[0-9]+pt\" height=\"[0-9]+pt\"\n viewBox=",
         "<svg width=\"100%\" height=\"100%\" id=\"mainSvgFile\" viewBox=",
         neatoLayout_graphSvg),
    "\n<script>",
    "var mainSvgFile = svgPanZoom('#mainSvgFile');",
    "</script>\n",
    "\n</body>\n</html>"
  );

### Write files

writeLines(dotLayout_html,
           here::here('public',
                      paste0(filenameBase, '_dot.html')));

writeLines(neatoLayout_html,
           here::here('public',
                      paste0(filenameBase, '_neato.html')));

DiagrammeR::export_graph(neatoLayout$output$graph,
                         here::here('public',
                                    paste0(filenameBase, '_neato.pdf')));

DiagrammeR::export_graph(dotLayout$output$graph,
                         here::here('public',
                                    paste0(filenameBase, '_dot.pdf')));

writeLines(dotLayout_graphSvg,
           here::here('public',
                      paste0(filenameBase, '_dot.svg')));

writeLines(neatoLayout_graphSvg,
           here::here('public',
                      paste0(filenameBase, '_neato.svg')));

```

<script>`r svgPanZoom`</script>

# Inleiding

Deze HTML pagina wordt door het script in de `scripts` directory van dit project (en dit Git repository) gegenereerd, en wordt gehost door GitLab Pages. Deze pagina is toegankelijk via `r gitLabPages` (de URL van dit Git repository zelf is `r gitLabRepo`).

Om deze pagina opnieuw te genereren, bijvoorbeeld omdat de informatie in de Google Sheet op `r googleSheetsURL` is veranderd, kun je naar `r gitLabJobs` gaan en op het ronde pijltje achter de bovenste 'job' klikken om die 'job' te herhalen.

Hiervoor moet je account wel geverifieerd zijn: de 'GitLab Runners' die die 'jobs' uitvoeren zijn gevoelig voor misbruik, en dit is een van de maatregelen die GitLab gebruikt om die risico's te beperken.

# "Neato" layout

<div style="width: 95%; height: 800px; border: 2px solid black;">
`r gsub("<svg width=\"[0-9]+pt\" height=\"[0-9]+pt\"\n viewBox=",
        "<svg width=\"100%\" height=\"100%\" id=\"neatoSvgFile\" viewBox=",
        neatoLayout_graphSvg)`
</div>
<script>
  var neatoSvgFile = svgPanZoom('#neatoSvgFile');
</script>

# "Dot" layout

<div style="width: 95%; height: 800px; border: 2px solid black;">
`r gsub("<svg width=\"[0-9]+pt\" height=\"[0-9]+pt\"\n viewBox=",
        "<svg width=\"100%\" height=\"100%\" id=\"dotSvgFile\" viewBox=",
        dotLayout_graphSvg)`
</div>
<script>
  var dotSvgFile = svgPanZoom('#dotSvgFile');
</script>

# Links naar losse bestanden

## Neato Layout

[HTML, Neato layout](`r filenameBase`_neato.html)

[PDF, Neato layout](`r filenameBase`_neato.pdf)

[SVG, Neato layout](`r filenameBase`_neato.svg)

## Dot Layout

[HTML, Dot layout](`r filenameBase`_dot.html)

[PDF, Dot layout](`r filenameBase`_dot.pdf)

[SVG, Dot layout](`r filenameBase`_dot.svg)
