# Intervention Mapping - Werkdruk in de Zorg

Deze HTML pagina wordt door het script in de scripts directory van dit project (en dit Git repository) gegenereerd, en wordt gehost door GitLab Pages. Deze pagina is toegankelijk via https://matherion.gitlab.io/im-werkdruk-in-de-zorg (de URL van dit Git repository zelf is https://gitlab.com/matherion/im-werkdruk-in-de-zorg).

Om deze pagina opnieuw te genereren, bijvoorbeeld omdat de informatie in de Google Sheet op https://docs.google.com/spreadsheets/d/1GEZl5rwlnMcHvY0PTZyaezl1PlzH1odRYrsNRf07xSg is veranderd, kun je naar https://gitlab.com/matherion/im-werkdruk-in-de-zorg/-/jobs gaan en op het ronde pijltje achter de bovenste ‘job’ klikken om die ‘job’ te herhalen.
